DROP DATABASE IF EXISTS dtx_account;
CREATE DATABASE dtx_account;

CREATE TABLE IF NOT EXISTS account (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `balance` bigint(20) NOT NULL COMMENT '用户余额，单位分',
    `freeze_amount` bigint(20) NOT NULL DEFAULT '0' COMMENT '冻结金额，扣款暂存余额，单位分',
    `last_update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;