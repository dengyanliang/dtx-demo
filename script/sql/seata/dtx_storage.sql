DROP DATABASE IF EXISTS dtx_storage;
CREATE DATABASE dtx_storage;

CREATE TABLE IF NOT EXISTS product(
    `id` bigint(20) NOT NULL AUTO_INCREMENT,
    `stock` bigint(20) NOT NULL DEFAULT '0' COMMENT '总库存',
    `freeze_stock` bigint(20) NOT NULL DEFAULT '0' COMMENT '冻结库存',
    `last_update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
    ) ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8;
INSERT INTO dtx_storage.product (id, stock) VALUES (1, 10); # 插入一条产品的库存