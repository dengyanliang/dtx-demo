package com.deng.seata.at.account.facade.request;


import lombok.Data;

import java.io.Serializable;

@Data
public class AccountRequest implements Serializable {
    private Integer userId;
    private Long amount;
}
