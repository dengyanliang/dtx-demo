package com.deng.seata.at.account.facade.response;

import lombok.Data;

import java.io.Serializable;

@Data
public class AccountResponse implements Serializable {
    private String code;
    private String message;
}
