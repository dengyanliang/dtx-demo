package com.deng.seata.at.order.dao.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.deng.seata.at.order.dao.po.Orders;

public interface OrdersMapper extends BaseMapper<Orders> {

}