package com.deng.seata.at.order;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ImportResource;

@EnableDubbo
@ImportResource(value = {"classpath*:springxml/*.xml"})
@MapperScan(basePackages = "com.deng.seata.at.order.dao.mapper")
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
// 将springboot应用程序注册到注册中心
@EnableDiscoveryClient
public class OrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class,args);
    }
}
