package com.deng.seata.at.order.service.impl;

import com.alibaba.fastjson.JSON;
import com.deng.seata.at.account.facade.AccountFacade;
import com.deng.seata.at.account.facade.request.AccountRequest;
import com.deng.seata.at.account.facade.response.AccountResponse;
import com.deng.seata.at.order.dao.mapper.OrdersMapper;
import com.deng.seata.at.order.dao.po.Orders;
import com.deng.seata.at.order.facade.request.OrderRequest;
import com.deng.seata.at.order.service.OrderService;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private AccountFacade accountFacade;

    @Resource
    private OrdersMapper ordersMapper;


    @Transactional(rollbackFor = Exception.class)
    @GlobalTransactional
    @Override
    public void addOrder(OrderRequest orderRequest) {


        log.info("事务ID------>{}", RootContext.getXID());

        // 新增订单
        Orders orders = new Orders();
        orders.setUserId(orderRequest.getUserId());
        orders.setProductId(orderRequest.getProductId());
        orders.setPayAmount(orderRequest.getAmount());
        orders.setPayStatus(1);
        orders.setCount(orderRequest.getCount());
        orders.setAddTime(new Date());
        orders.setLastUpdateTime(new Date());
        int addCount = ordersMapper.insert(orders);
        if(addCount <= 0){
            throw new RuntimeException("添加订单失败");
        }

        AccountRequest request = new AccountRequest();
        request.setUserId(orderRequest.getUserId());
        request.setAmount(orderRequest.getAmount());
        log.info("调用账户请求 request:{}", JSON.toJSONString(request));
        AccountResponse response = accountFacade.transfer(request);
        log.info("调用账户响应 response:{}", JSON.toJSONString(response));

        int i =  10 / 0;
    }
}
