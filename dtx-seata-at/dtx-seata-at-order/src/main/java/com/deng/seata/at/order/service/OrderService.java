package com.deng.seata.at.order.service;


import com.deng.seata.at.order.facade.request.OrderRequest;

public interface OrderService {
    void addOrder(OrderRequest orderRequest);
}
