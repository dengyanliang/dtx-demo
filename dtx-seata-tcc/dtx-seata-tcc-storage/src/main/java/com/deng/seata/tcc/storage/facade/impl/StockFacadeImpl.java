package com.deng.seata.tcc.storage.facade.impl;

import com.alibaba.fastjson.JSON;
import com.deng.seata.tcc.storage.facade.StockFacade;
import com.deng.seata.tcc.storage.facade.request.StockRequest;
import com.deng.seata.tcc.storage.facade.response.StockResponse;
import com.deng.seata.tcc.storage.service.StockService;
import com.deng.seata.tcc.storage.utils.ResultHolder;
import io.seata.core.context.RootContext;
import io.seata.rm.tcc.api.BusinessActionContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Slf4j
@Component("stockFacade")
public class StockFacadeImpl implements StockFacade {

    @Resource
    private StockService stockService;


    @Override
    public StockResponse tryFreezeStock(StockRequest request) {
        String txNo = RootContext.getXID(); // 全局事务id
        log.info("freezeStock try 开始执行，txNo: {}", txNo);

//        if(Objects.nonNull(tccLocalTryLogMapper.selectByPrimaryKey(txNo))){
//            log.info("freezeStock try 已经执行，无需重复执行...txNo:{}",txNo);
//            return;
//        }
//
//        // try悬挂判断，如果confirm、cancel有一个已经执行了，try不再执行  todo 这里应该只用判断cancel的吧？
//        if(Objects.nonNull(tccLocalConfirmLogMapper.selectByPrimaryKey(txNo)) || Objects.nonNull(tccLocalCancelLogMapper.selectByPrimaryKey(txNo))){
//            log.info("freezeStock try悬挂处理，confirm或者cancel有一个已经执行了，try不能再执行，txNo:{}",txNo);
//            return;
//        }

        stockService.tryFreeze(request);

        // 添加try日志
//        TccLocalTryLog tryLog = new TccLocalTryLog();
//        tryLog.setTxNo(txNo);
//        tryLog.setCreateTime(new Date());
//        tccLocalTryLogMapper.insert(tryLog);

        log.info("freezeStock try 执行结束，txNo:{}",txNo);

        StockResponse stockResponse = new StockResponse();
        stockResponse.setCode("1000");
        stockResponse.setMessage("成功");
        return stockResponse;
    }

    public void commitFreezeStock(BusinessActionContext actionContext) {
        String xid = actionContext.getXid(); // 全局事务id
        log.info("commitFreezeStock 开始执行，txNo:{}",xid);
        // 防幂等控制
        String actionOneResult = ResultHolder.getActionResult(xid);
        if(StringUtils.isNotBlank(actionOneResult)){
            log.info("》》》》commitFreezeStock 已经提交过了, xid: {}",xid);
            return;
        }
        ResultHolder.setActionResult(xid,"T");

        String requestString = actionContext.getActionContext("stockRequest").toString();
        log.info("》》》》commitFreezeStock 请求信息：{}", requestString);
        StockRequest stockRequest = JSON.parseObject(requestString, StockRequest.class);

        stockService.commitFreeze(stockRequest);

        log.info("commitFreezeStock 执行结束，txNo:{}",xid);
    }


    public void cancelFreezeStock(BusinessActionContext actionContext){
        String xid = actionContext.getXid(); // 全局事务id
        log.info("cancelFreezeStock 开始执行，txNo:{}", xid);
        // 防幂等控制
        String actionOneResult = ResultHolder.getActionResult(xid);
        if(StringUtils.isNotBlank(actionOneResult)){
            log.info("》》》》cancelFreezeStock 已经提交过了, xid: {}",xid);
            return;
        }
        ResultHolder.setActionResult(xid,"C");

        String requestString = actionContext.getActionContext("stockRequest").toString();
        log.info("》》》》cancelFreezeStock 请求信息：{}", requestString);
        StockRequest stockRequest = JSON.parseObject(requestString, StockRequest.class);

        stockService.cancelFreeze(stockRequest);

        log.info("cancelFreezeStock 执行结束，txNo:{}",xid);
    }

}
