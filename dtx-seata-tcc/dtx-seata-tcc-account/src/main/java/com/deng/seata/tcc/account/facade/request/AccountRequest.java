package com.deng.seata.tcc.account.facade.request;


import lombok.Data;

import java.io.Serializable;

@Data
public class AccountRequest implements Serializable {
    private Integer userId;
    private Long amount;
}
