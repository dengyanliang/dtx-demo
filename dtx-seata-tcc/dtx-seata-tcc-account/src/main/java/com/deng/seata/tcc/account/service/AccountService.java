package com.deng.seata.tcc.account.service;


import com.deng.seata.tcc.account.facade.request.AccountRequest;

public interface AccountService {
    void transfer(AccountRequest request);
    void transferCommit(AccountRequest request);
    void transferCancel(AccountRequest request);
}
