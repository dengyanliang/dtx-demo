package com.deng.seata.tcc.account.facade.response;

import lombok.Data;

import java.io.Serializable;

@Data
public class AccountResponse implements Serializable {
    private String code;
    private String message;
}
