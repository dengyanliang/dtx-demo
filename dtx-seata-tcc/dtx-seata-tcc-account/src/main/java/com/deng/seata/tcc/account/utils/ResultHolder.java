/*
 *  Copyright 1999-2021 Seata.io Group.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.deng.seata.tcc.account.utils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 防幂等控制，生产环境，需要用redis进行分布式控制
 */
public class ResultHolder {

    private static Map<String, String> actionResults = new ConcurrentHashMap<String, String>();

    /**
     * 把事务请求信息放入到集合中
     * @param txId   分支事务id
     * @param result 放入的对象
     */
    public static void setActionResult(String txId, String result) {
        actionResults.put(txId, result);
    }

    /**
     * 把事务请求信息放入到集合中
     * @param txId   分支事务id
     * @param result 放入的对象
     */
    public static String getActionResult(String txId) {
        return actionResults.get(txId);
    }


}
