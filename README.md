#### 环境
jdk8、mysql

#### 介绍
Seata 是一款开源的分布式事务解决方案，致力于提供高性能和简单易用的分布式事务服务。
Seata 将为用户提供了 AT、TCC、SAGA 和 XA 事务模式，为用户打造一站式的分布式解决方案。

#### 安装教程
1.  新建数据库seata、dtx_account、dtx_order、dtx_storage，分别到对应的数据库中执行script/sql/seata目录下的文件seata.sql、dtx_account.sql、dtx_order.sql、dtx_storage.sql

2. 下载nacos-server-2.2.3（https://github.com/alibaba/nacos/releases）
   解压nacos-server-2.2.3.zip，运行nacos服务器，由于是单机启动，所以要加上-m standalone模式
   mac sh bin/startup.sh -m standalone
   win sh bin/startup.cmd -m standalone

3. 到seata官网，下载seata-server-1.8.0.zip并解压，修改conf目录下的application.yml
~~~text
server:
  port: 7091

console:
  user:
    username: seata
    password: seata


spring:
  application:
    name: seata-server

logging:
  config: classpath:logback-spring.xml
  file:
    path: ${log.home:${user.home}/logs/seata}
  extend:
    logstash-appender:
      destination: 127.0.0.1:4560
    kafka-appender:
      bootstrap-servers: 127.0.0.1:9092
      topic: logback_to_logstash

seata:
  security:
    secretKey: seata
    tokenValidityInMilliseconds: 1000000000
  config:
    # support: nacos 、 consul 、 apollo 、 zk  、 etcd3
    type: nacos
    nacos:
      server-addr: 127.0.0.1:8848
      namespace:
      group: SEATA_GROUP
      username:
      password:
      context-path:
      ##if use MSE Nacos with auth, mutex with username/password attribute
      #access-key:
      #secret-key:
      data-id: seataServer.properties
  registry:
    # support: nacos 、 eureka 、 redis 、 zk  、 consul 、 etcd3 、 sofa
    type: nacos
    preferred-networks: 30.240.*
    nacos:
      application: seata-server
      server-addr: 127.0.0.1:8848
      group: SEATA_GROUP
      namespace:
      cluster: default-tx
      username:
      password:
      context-path:

  server:
    service-port: 8091 #If not configured, the default is '${server.port} + 1000'
    max-commit-retry-timeout: -1
    max-rollback-retry-timeout: -1
    rollback-retry-timeout-unlock-enable: false
    enable-check-auth: true
    enable-parallel-request-handle: true
    retry-dead-threshold: 130000
    xaer-nota-retry-timeout: 60000
    enableParallelRequestHandle: true
    recovery:
      committing-retry-period: 1000
      async-committing-retry-period: 1000
      rollbacking-retry-period: 1000
      timeout-retry-period: 1000
    undo:
      log-save-days: 7
      log-delete-period: 86400000
    session:
      branch-async-queue-size: 5000 #branch async remove queue size
      enable-branch-async-remove: false #enable to asynchronous remove branchSession
  store:
    # support: file 、 db 、 redis
    mode: db
    session:
      mode: db
    lock:
      mode: db
    db:
      datasource: druid
      db-type: mysql
      driver-class-name: com.mysql.jdbc.Driver
      url: jdbc:mysql://127.0.0.1:3306/seata?rewriteBatchedStatements=true
      user: mysql
      password: mysql
      min-conn: 10
      max-conn: 100
      global-table: global_table
      branch-table: branch_table
      lock-table: lock_table
      distributed-lock-table: distributed_lock
      query-limit: 1000
      max-wait: 5000
  metrics:
    enabled: false
    registry-type: compact
    exporter-list: prometheus
    exporter-prometheus-port: 9898
  transport:
    rpc-tc-request-timeout: 15000
    enable-tc-server-batch-send-response: false
    shutdown:
      wait: 3
    thread-factory:
      boss-thread-prefix: NettyBoss
      worker-thread-prefix: NettyServerNIOWorker
      boss-thread-size: 1
~~~

如果是saga模式，运行 http://localhost:7091/#/sagastatemachinedesigner 可以编写 saga状态机

4. 运行seata服务器，
mac sh bin/seata-server.sh 
win 双击bin目录下的seata-server.bat

5. 参考 
seata文档：https://seata.apache.org/zh-cn/

官方demo：https://github.com/apache/incubator-seata-samples.git

状态机样例：
https://github.com/apache/incubator-seata/tree/refactor_designer/saga/seata-saga-statemachine-designer

脚本：
使用 DB 存储模式时，需要注意使用相应 seata-server 对应版本的建表脚本，建表脚本获取地址：https://github.com/apache/incubator-seata/tree/${版本}/script/server/db，例如：获取seata-server 1.5.0 对应的建表脚本，可从此地址获取 https://github.com/apache/incubator-seata/tree/1.5.0/script/server/db 升级 seata-server 前需要先变更表结构。
